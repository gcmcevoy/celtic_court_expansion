########################################################
## LEGENDARY BARD EVENTS FOR ANCIENT RELIGIONS REBORN ##
########################################################

namespace = LBE

#Create Bard
character_event = {
	id = LBE.600
	hide_window = yes
	min_age = 16
	capable_only = yes
	only_playable = yes
	prisoner = no

	trigger = {
		completely_controls = d_fianna
		NOR = {
			has_game_rule = {
				name = supernatural_events
				value = off
			}
			has_character_flag = legendary_bard_saga
		}
		in_command = no
		OR = {
			religion = celtic_pagan
			religion = celtic_pagan_reformed
		}
	}
	
	mean_time_to_happen = {
		months = 5000
	}	
	
	immediate = {
		set_character_flag = legendary_bard_saga
		any_courtier_or_vassal = {
			limit = {
				liege = { character = ROOT }
				has_minor_title = title_court_bard
			}
			save_event_target_as = current_bard
		}
		create_character = {
			random_traits = no
			female = no
			dynasty = NONE
			age = 73
			religion = ROOT
			culture = irish
			trait = naive_appeaser
			trait = fair
			trait = fluent
			trait = shrewd
			trait = adventurer
			#trait = socializer
			trait = poet
			trait = proud
			trait = kind
			trait = brave
			trait = gregarious
			trait = trusting
			#trait = blinded
			trait = one_eyed
			trait = inspiring_leader
			attributes = {
						diplomacy = 10
						martial = 6
						learning = 6
					}
			health = 15
			fertility = 0
			flag = ai_flag_refuse_conversion
		}
		new_character = {
			set_character_flag = legendary_bard 
			give_nickname = nick_the_bard
			save_event_target_as = legendary_bard
			character_event = { id = LBE.601 }
			will_not_marry_effect = yes
		}
	}
}
# Bard Ping
character_event = {
	id = LBE.601
	hide_window = yes
	is_triggered_only = yes

	immediate = {
		FROM = { character_event = { id = LBE.602 } }
	}
}

# Notify player of Bard Appearance
character_event = {
	id = LBE.602
	picture = GFX_evt_feast
	border = GFX_event_normal_frame_religion
	is_triggered_only = yes
	
	desc = EVTDESC_LBE_602
	
	option = { # Accept the wanderer as your court bard
		name = EVTOPTA_LBE_602
		ai_chance = {
			factor = 0
		}
		hidden_tooltip = {
			if = { 
				limit = {
					any_courtier_or_vassal = {
						liege = { character = ROOT }
						has_minor_title = title_court_bard
					}
				}
				any_courtier_or_vassal = {
					limit = {
						liege = { character = ROOT }
						has_minor_title = title_court_bard
					}
					remove_title = title_court_bard
				}
			}
		}
		transfer_scaled_wealth = {
			to = event_target:legendary_bard
			min = 15
			value = 0.1
		}
		event_target:legendary_bard = {
			opinion = {
				modifier = opinion_accepted_offer
				who = ROOT
				years = 5
			}
			give_minor_title = title_court_bard
			character_event = { id = LBE.6031 days = 7 }
			#character_event = { id = LBE.6031 days = 90 random = 14 }
		}
	}
	option = { # tell him to hit the road
		name = EVTOPTB_LBE_602
		custom_tooltip = { text = away_bard }
		if = {
			limit = { trait = kind }
			random = {
				chance = 33
				remove_trait = kind
			}
		}
		hidden_tooltip = {
			event_target:legendary_bard = { 
				death = {
					death_reason = death_missing
				}
			}
		}
	}
	option = { # I already have a bard, silly
		name = EVTOPTC_LBE_602
		trigger = {
			diplomacy = 10
			any_courtier_or_vassal = {
				liege = { character = ROOT }
				has_minor_title = title_court_bard
			}
		}
		tooltip_info = diplomacy
		event_target:current_bard = {
			opinion = {
				modifier = opinion_respite
				who = ROOT
				years = 3
			}
		}
		if = {
			NOT = { trait = just }
			random = {
				chance = 33
				add_trait = just
			}
		}
		hidden_tooltip = {
			event_target:legendary_bard = { 
				death = {
					death_reason = death_missing
				}
			}
		}
	}
}

#######################################
## Bard wants to know if you're down ##
#######################################

# Bard Ping
character_event = {
	id = LBE.6031
	hide_window = yes
	is_triggered_only = yes

	immediate = {
		FROM = { character_event = { id = LBE.603 } }
	}
}

# Actual Event
character_event = {
	id = LBE.603
	picture = GFX_evt_stressed_ruler
	is_triggered_only = yes
	
	desc = EVTDESC_LBE_603
	
	option = {
		name = EVTOPTA_LBE_603
		custom_tooltip = { text = EVTTTPA_LBE_603 }
		hidden_tooltip = {
			event_target:legendary_bard = {
				character_event = { id = LBE.6041 days = 7 }
			}
		}
		random = {
			chance = 50
			add_trait = stressed
		}
	}
	option = {
		name = EVTOPTB_LBE_603
		custom_tooltip = { text = EVTTTPB_LBE_603 }
	}
	option = {
		name = EVTOPTC_LBE_603
		event_target:legendary_bard = {
			opinion = {
				modifier = opinion_snapped
				who = ROOT
				years = 5
			}
		}
	}
}

#######################################
## Bard has some crazy stories 4 you ##
#######################################

# Bard Ping
character_event = {
	id = LBE.6041
	hide_window = yes
	
	is_triggered_only = yes

	immediate = {
		FROM = { character_event = { id = LBE.604 } }
	}
}

character_event = {
	id = LBE.604
	picture = GFX_evt_throne_room
	is_triggered_only = yes
	desc = EVTDESC_LBE_604
	
	option = {
		name = EVTOPTA_LBE_604
		hidden_tooltip = {
			ROOT = {
				character_event = { id = LBE.699 }
			}
		}
	}
	option = {
		name = EVTOPTB_LBE_604
		hidden_tooltip = {
			ROOT = {
				character_event = { id = LBE.699 }
			}
		}
	}
	option = {
		name = EVTOPTC_LBE_604
		custom_tooltip = { name = EVTTTPB_LBE_603 }
	}
	option = {
		trigger = {
			intrigue = 15
		}
		name = EVTOPTD_LBE_604
		tooltip_info = intrigue
		FROM = {
			opinion = {
				modifier = opinion_snapped
				who = ROOT
				years = 5
			}
		}
		#hidden_tooltip = {
		#	FROM = {
		#		character_event = { id = LBE.607 }
		#	}
		#}
	}
}
# Bard Ping, Round 2 - Yes
character_event = {
	id = LBE.605
	hide_window = yes
	is_triggered_only = yes

	immediate = {
		FROM = { character_event = { id = LBE.608 } }
	}
}
# Bard Ping, Round 2 - No
character_event = {
	id = LBE.606
	hide_window = yes
	is_triggered_only = yes

	immediate = {
		FROM = { character_event = { id = LBE.609 } }
	}
}
# Bard Ping, Round 2 - Don't Yell at Me
character_event = {
	id = LBE.607
	hide_window = yes
	is_triggered_only = yes

	immediate = {
		FROM = { character_event = { id = LBE.610 } }
	}
}


#################
## I am Oisin! ##
#################
character_event = {
	id = LBE.699
    hide_window = yes

	is_triggered_only = yes
	
	immediate = {
        any_courtier_or_vassal = { 
            limit = { has_character_flag = legendary_bard }
            character_event = { id = LBE.7001 }
        }
    }
}
#Bard Ping
character_event = {
    id = LBE.7001
        hide_window = yes

        is_triggered_only = yes

        immediate = {
            FROM = { character_event = { id = LBE.700 } }
        }
}

character_event = {
    id = LBE.700
        picture = GFX_evt_throne_room
        is_triggered_only = yes
        desc = EVTDESC_LBE_700

    option = {
        name = EVTOPTA_LBE_700
            FROM = {
                set_name = Ois�n
                add_claim = d_fianna
            }
        hidden_tooltip = {
            FROM = {
                remove_trait = naive_appeaser
                add_trait = brilliant_strategist
                add_trait = fennid
                change_diplomacy = 1
                add_artifact = ceard_nan_gallan
            }
        }
        ROOT = {
            prestige = -100
            piety = -100
            random_list = {
                80 = {
                add_trait = lunatic
                }
                20 = {
                add_trait = zealous
                }
            }
        }
		FROM = {
			opinion = {
				modifier = believed_story
				who = ROOT
				years = 20
			}
		}
	}
	option = {
		name = EVTOPTB_LBE_700
		trigger = {
			trait = zealous
		}
		tooltip_info = zealous
		FROM = {
			set_name = Ois�n
			give_title = d_fianna # add_claim or give_title?
		}
		hidden_tooltip = {
			FROM = {
				remove_trait = naive_appeaser
				add_trait = brilliant_strategist
				add_trait = fennid
				change_diplomacy = 1
			}
		}
		ROOT = {
			prestige = -100
			piety = -100
			random_list = {
				80 = {
				add_trait = lunatic
				}
				20 = {
				add_trait = zealous
				}
			}
		}
		FROM = {
			opinion = {
				modifier = believed_story
				who = ROOT
				years = -1
			}
		}
	}
	option = {
		name = EVTOPTC_LBE_700
		ROOT = {
			prestige = 100
			piety = 100
		}
		FROM = {
			add_trait = lunatic
			add_trait = depressed
			opinion = {
				modifier = did_not_believe_story
				who = ROOT
				years = -1
			}
		}
	}
}
