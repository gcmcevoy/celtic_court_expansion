cu_chulainn = {
	martial = 2
	combat_rating = 5
	liege_opinion = 5
	monthly_character_prestige = 0.5

	inheritance = patrilineal
	allow_bastards = yes
	picture = GFX_bloodlines_symbol_generic_pagan
	flags = { created_bloodline }
	active = {
		culture_group = celtic
	}
}
